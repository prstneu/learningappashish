//
//  URLSessionDataTaskResponse.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

enum FetchError {
    case json(String)
    case jsonCast
    case parsing
    case emptyResponse
    case network(String)
}

enum URLSessionResult<T> {
    case success(T)
    case failure(FetchError)
}

struct URLSessionDataTaskResponse<T> {
    let serializeJSON: Bool?
    let parse: (Any) -> T?
    func handle(data: Data?, error: Error?) -> URLSessionResult<T> {
        guard let data = data else {
            if let error = error {
                return .failure(.network(error.localizedDescription))
            }
            else {
                return .failure(.emptyResponse)
            }
        }
        do {
            let serializeObject: Any?
            if serializeJSON == true {
                serializeObject = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            else {
                serializeObject = data
            }
            if let serializeObject = serializeObject {
                if let parsedObject = parse(serializeObject) {
                    return .success(parsedObject)
                }
                else {
                    return .failure(.parsing)
                }
            }
            else {
                return .failure(.jsonCast)
            }
            
        } catch let searializationError {
            return .failure(.json(searializationError.localizedDescription))
        }
    }

}
