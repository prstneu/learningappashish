//
//  File.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

protocol ASRequest {
    var baseURL: URL? { get }
    var method: String { get }
    var path: String { get }
    var parameters: [String: String] { get }
    var headers: [String: String] { get }
}

extension ASRequest {
    var method : String { return "GET" }
    var baseURL: URL? { return URL(string: "https://api.spotify.com/v1", relativeTo: nil) }
    var headers: [String : String] {
        return ["Authorization": "Bearer BQCbDjrtpDB6PTBv7QcCL1876RszsYVrM7kiw3LTHEOizvnjQCrrzVcs2BVDMgMHqx3JBddApDdn2Uhm5BjrwACVHTVWiiIrbgoXIE6gpxXJc_L8h_mqd-UWGyvteGRlb1JqS9Jk6wVIN1s"]
    }
    var parameters: [String: String] { return Dictionary() }
}

extension ASRequest {
    func buildRequest() -> URLRequest? {
        guard let baseURL = baseURL else {
            return nil
        }
        guard var URLComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else {
            return nil
        }
        URLComponents.path = URLComponents.path + path
    
        if method != "POST" {
            let queryItems: [URLQueryItem] = parameters.map { (key, value) in
                return URLQueryItem(name: key, value: value)
            }
            URLComponents.queryItems = queryItems
        }
        guard let URL = URLComponents.url else {
            return nil
        }
        var request = URLRequest(url: URL)
        request.httpMethod = method
        if method == "POST" && parameters.values.count > 0 {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        return request
    }
}
