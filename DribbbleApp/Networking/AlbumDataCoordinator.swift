//
//  PinDataCoordinator.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation
import UIKit

protocol AlbumCoordinatorDelegate: class {
    func didFetchAlbums(coordinator: AlbumDataCoordinator, albums: [Album]?, error: ErrorViewModel?)
}

class AlbumDataCoordinator {
    weak var delegate: AlbumCoordinatorDelegate?
    let albumRequest = AlbumRequest()
    let responseHandler = URLSessionDataTaskResponse(serializeJSON: true) {
        (json: Any) -> [Album]? in
        var spotifyAlbums: [Album]?
        if let json = json as? [String: Any] {
            if let albums = json["items"] as? [ [String: Any] ] {
                spotifyAlbums = albums.compactMap({ (json: [String: Any]) -> Album? in
                    return Album.fromJSON(json: json)
                })
            }
        }
        else {
            spotifyAlbums = nil
        }
        return spotifyAlbums
    }
    
    func getAlbums() {
        let task = URLSession.shared.fetch(urlRequest: albumRequest.buildRequest()!, responseHandler: responseHandler) {
            [weak self] (result: URLSessionResult) in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                switch(result) {
                case let .success(albums):
                    self?.delegate?.didFetchAlbums(coordinator: strongSelf, albums: albums, error: nil)
                case let .failure(error):
                    self?.delegate?.didFetchAlbums(coordinator: strongSelf, albums: nil, error: ErrorViewModel.getErrorViewModel(fromFetchError: error))
                }
            }
        }
        task.resume()
    }
    
}

