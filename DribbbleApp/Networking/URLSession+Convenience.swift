//
//  URLSession+Convenience.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

extension URLSession {
    func fetch<T>(
        urlRequest: URLRequest,
        responseHandler: URLSessionDataTaskResponse<T>,
        completion: @escaping (URLSessionResult<T>) -> Void
        ) -> URLSessionDataTask {
        let task = dataTask(with: urlRequest) { (data, response, error) in
            let result = responseHandler.handle(data: data, error: error)
            completion(result)
        }
        return task
    }
}
