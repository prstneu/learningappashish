//
//  PinRequest.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

struct AlbumRequest: ASRequest {
    var path: String {
        return "/artists/4DFhHyjvGYa9wxdHUjtDkc/albums"
    }
    var parameters: [String: String] {
        return [:]
    }
}

struct AuthRequest: ASRequest {
    var baseURL: URL? {
        return URL(string: "https://accounts.spotify.com/authorize")
    }
    var path: String {
        return ""
    }
    var parameters: [String: String] {
        return ["client_id": "c38c51eb1fb7479c99dbcf8e0a1235c1",
                "response_type": "code",
                "redirect_uri": "https://mysporityfapp",
                "state": "123"]
    }
}

struct AuthToken: ASRequest {
    let code: String
    
    var baseURL: URL? {
        return URL(string: "https://accounts.spotify.com/api/token")
    }
    var path: String {
        return ""
    }
    var parameters: [String: String] {
        return ["grant_type": "authorization_code",
                "redirect_uri": "https://mysporityfapp",
                "code": code]
    }
    var method: String {
        return "POST"
    }
    
    var headers: [String : String] {
        return ["Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Basic YzM4YzUxZWIxZmI3NDc5Yzk5ZGJjZjhlMGExMjM1YzE6NDc3ZjFkYjJlYjRmNGM4MDlkOTVmNmZhNTcyMDAxMmU="]
    }
    func buildRequest() -> URLRequest? {
        guard let baseURL = baseURL else {
            return nil
        }
        guard var URLComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else {
            return nil
        }
        URLComponents.path = URLComponents.path + path
        let queryItems: [URLQueryItem] = parameters.map { (key, value) in
            return URLQueryItem(name: key, value: value)
        }
        URLComponents.queryItems = queryItems
        
        guard let URL = URLComponents.url else {
            return nil
        }
        var request = URLRequest(url: URL)
        request.httpMethod = method
        
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        return request
    }
    
    init(code: String) {
        self.code = code
    }
}

struct RefreshToken: ASRequest {
    var baseURL: URL? {
        return URL(string: "https://accounts.spotify.com/api/token")
    }
    var path: String {
        return ""
    }
    var parameters: [String: String] {
        return ["grant_type": "refresh_token",
                 "refresh_token": refreshToken]
    }
    var headers: [String : String] {
        return ["Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Basic YzM4YzUxZWIxZmI3NDc5Yzk5ZGJjZjhlMGExMjM1YzE6NDc3ZjFkYjJlYjRmNGM4MDlkOTVmNmZhNTcyMDAxMmU="]
    }
    
    var method: String {
        return "POST"
    }
    
    func buildRequest() -> URLRequest? {
        guard let baseURL = baseURL else {
            return nil
        }
        guard var URLComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else {
            return nil
        }
        URLComponents.path = URLComponents.path + path
        let queryItems: [URLQueryItem] = parameters.map { (key, value) in
            return URLQueryItem(name: key, value: value)
        }
        URLComponents.queryItems = queryItems
        
        guard let URL = URLComponents.url else {
            return nil
        }
        var request = URLRequest(url: URL)
        request.httpMethod = method
        
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        return request
    }
}
