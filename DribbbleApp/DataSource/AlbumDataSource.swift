//
//  AlbumDataSource.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 28/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation
import UIKit
class AlbumDataSource {
    let tableView: UITableView
    let albums: [Album]
    var cache = NSCache<NSString, NSNumber>()
    init(tableView: UITableView, albums: [Album]) {
        self.tableView = tableView
        self.albums = albums
    }
    
    lazy var prototypeCell = tableView.dequeueReusableCell(withIdentifier: AlbumTableViewCell.albumTableViewCellIdentifier) as! AlbumTableViewCell
    
    func heightForRowAtIndexPath(indexpath: IndexPath) -> CGFloat {
        
        if let name = albums[indexpath.row].name,
            let height = cache.object(forKey: NSString(string: name + "\(indexpath.row)")) {
            return CGFloat(height.floatValue)
        }
        
        var totalHeight = tableView.bounds.width - 40
        if let name = albums[indexpath.row].name {
            let random = indexpath.row % 6
            let artistname: String = AlbumTableViewCell.texts[random]
            totalHeight += artistname.height(withConstrainedWidth: tableView.bounds.width - 40, font: prototypeCell.albumNameLabel.font)
        }
        
        
        totalHeight += "artistname".height(withConstrainedWidth: tableView.bounds.width - 40, font: prototypeCell.artistNameLabel.font)
        totalHeight += 30 + 8
        cache.setObject(NSNumber(value: Float(totalHeight)), forKey: NSString(string: albums[indexpath.row].name! + "\(indexpath.row)"))
        return totalHeight
        
        prototypeCell.configureCell(album: albums[indexpath.row], row: indexpath.row)
        prototypeCell.setNeedsLayout()
        prototypeCell.layoutIfNeeded()
        
        let height = prototypeCell.contentView.systemLayoutSizeFitting(CGSize(width: tableView.bounds.width, height: CGFloat.infinity), withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow).height + 1
        if let name = albums[indexpath.row].name {
            cache.setObject(NSNumber(value: Float(height)), forKey: NSString(string: name + "\(indexpath.row)"))
        }
        return height
    }
}
