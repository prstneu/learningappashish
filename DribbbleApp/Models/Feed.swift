//
//  Feed.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

struct Feed {
    var albums: [Album] = []
    init(albums: [Album]) {
        self.albums = albums
    }
    
    func appendAlbums(albums: [Album]) -> Feed {
        let pastAlbums = self.albums + albums
        return Feed(albums: pastAlbums)
    }
}
