//
//  SpotifyImage.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation
import UIKit
struct SpotifyImage: JSONConvertible {
    let height: CGFloat?
    let width: CGFloat?
    let url: String?
    static func fromJSON(json: [String : Any]) -> SpotifyImage? {
        let height: CGFloat?
        let width: CGFloat?
        let url: String?
        if let imageHeight = json["height"] as? CGFloat {
            height = imageHeight
        }
        else {
            height = nil
        }
        if let imageWidth = json["width"] as? CGFloat {
            width = imageWidth
        }
        else {
            width = nil
        }
        if let imageURL = json["url"] as? String {
            url = imageURL
        }
        else {
            url = nil
        }
        return SpotifyImage(height: height, width: width, url: url)
    }
}
