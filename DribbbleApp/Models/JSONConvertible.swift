//
//  JSONConvertible.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 26/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

protocol JSONConvertible {
   static func fromJSON(json: [String: Any]) -> Self?
}
