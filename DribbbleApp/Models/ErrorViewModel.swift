//
//  ErrorViewModel.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 28/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

enum ErrorTypes {
    case fetchError
}
struct ErrorViewModel {
    let title: String
    let message: String
    let type: ErrorTypes
    
    static func getErrorViewModel(fromFetchError: FetchError) -> ErrorViewModel {
        let title = "Something went wrong"
        let message: String
        switch fromFetchError {
        case .emptyResponse:
            message = "Empty Response. Try again in some time"
        case .jsonCast:
            message = "Wrong format obtained from respponse"
        case .parsing:
            message = "Parsing error."
        case let .json(msg):
            message = msg
        case let .network(msg):
            message = msg
        }
        return ErrorViewModel(title: title, message: message, type: .fetchError)
    }
}
