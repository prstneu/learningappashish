//
//  Album.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

struct Album: JSONConvertible {
    let artists: [Artist]?
    let genres: [String]?
    let images: [SpotifyImage]?
    let label: String?
    let name: String?
    static func fromJSON(json: [String : Any]) -> Album? {
        let artists: [Artist]?
        let genres: [String]?
        let images: [SpotifyImage]?
        let label: String?
        let name: String?
       
        if let albumArtists = json["artists"] as? [ [String: Any] ] {
            artists = albumArtists.compactMap({ (json: [String: Any]) -> Artist? in
                return Artist.fromJSON(json: json)
            })
        }
        else {
            artists = nil
        }
        
        if let albumImages = json["images"] as? [ [String: Any] ] {
            images = albumImages.compactMap({ (json: [String: Any]) -> SpotifyImage? in
                return SpotifyImage.fromJSON(json: json)
            })
        }
        else {
            images = nil
        }
        
        if let albumGenres = json["genres"] as? [String] {
            genres = albumGenres
        }
        else {
            genres = nil
        }
        
        if let albumLabel = json["label"] as? String {
            label = albumLabel
        }
        else {
            label = nil
        }
        
        if let albumName = json["name"] as? String {
            name = albumName
        }
        else {
            name = nil
        }
        
        return Album(artists: artists,
                     genres: genres,
                     images: images,
                     label: label,
                     name: name)
    }
}
