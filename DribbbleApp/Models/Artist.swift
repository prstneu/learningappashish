//
//  Artist.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 27/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation

struct Artist: JSONConvertible {
    let name: String?
    let type: String?
    static func fromJSON(json: [String : Any]) -> Artist? {
        let name: String?
        let type: String?
        if let artistName = json["name"] as? String {
            name = artistName
        }
        else {
            name = nil
        }
        if let artistType = json["type"] as? String {
            type = artistType
        }
        else {
            type = nil
        }
        return Artist(name: name, type: type)
    }
}
