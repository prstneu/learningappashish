//
//  String+Convenience.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 29/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation
import UIKit
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingBox.height)
    }
}
