//
//  NSAttributedString+Convenience.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 29/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
}
