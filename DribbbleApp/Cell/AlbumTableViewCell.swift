//
//  AlbumTableViewCell.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 28/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    static let albumTableViewCellIdentifier = "AlbumTableViewCell"
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func configureCell(album: Album, row: Int) {
        if let imageObjects = album.images, imageObjects.count > 1 {
            albumImageView.imageURL = imageObjects[1].url
        }
        if let artists = album.artists, artists.count > 0 {
            artistNameLabel.text = artists[0].name
        }
         let random = row % 6
        albumNameLabel.text = AlbumTableViewCell.texts[random]
    }
    
    static let texts = ["Welcome to Medium, a place to read, write, and interact with the stories that matter most to you. Every day, thousands of voices read, write, and share important stories on Medium",
                 "Every week on Spiralbound we ask one of our featured artists: How do you make comics? This week we spoke with Hilary Fitzgerald Campbell, of the wonderful “Edited Story of Us.” (See here for our full archive.)",
                 "So, Hilary, how do you make comics? (Text from Hilary, drawings by Edith Z.)",
                 "Medium member since Sep 2017 Spiralbound Sep 2017 Spiralbound Sep 2017 Spiralbound",
                 "Love it. I’m a follower of Scott McCloud who talks a lot about how sequential art can be used for pieces other than just “comics”, like interviews, research, news articles, etc. It was refreshing to see you do just that. Love it. I’m a follower of Scott McCloud who talks a lot about how sequential art can be used for pieces other than just “comics”, like interviews, research, news articles, etc. It was refreshing to see you do just that.",
                 "Love it. I’m a follower of Scott"]
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        albumImageView.imageURL = nil
    }
}
