//
//  ViewController.swift
//  DribbbleApp
//
//  Created by Ashish Singh on 26/04/18.
//  Copyright © 2018 Ashish Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate, AlbumCoordinatorDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    var webView = UIWebView(frame: CGRect.zero)
    var albums: [Album] = []
    var coordinator: AlbumDataCoordinator = AlbumDataCoordinator()
    lazy var dataSource: AlbumDataSource = AlbumDataSource(tableView: tableView, albums: albums)
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableView.dataSource = self
        tableView.delegate = self
        //tableView.estimatedRowHeight = 300
        //tableView.rowHeight = UITableViewAutomaticDimension
        let cellNib = UINib(nibName: AlbumTableViewCell.albumTableViewCellIdentifier, bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: AlbumTableViewCell.albumTableViewCellIdentifier)
        tableView.separatorStyle = .none
        /*let req = RefreshToken()
        let responseHandlerr = URLSessionDataTaskResponse(serializeJSON: true) {
            (json: Any) -> Any in
            print(json)
        }
        let taskk = URLSession.shared.fetch(urlRequest: req.buildRequest()!, responseHandler: responseHandlerr) { (result) in
            print(result)
        }
        taskk.resume()
        return*/
        coordinator.delegate = self
        coordinator.getAlbums()
        return
        webView.frame = view.bounds
        view.addSubview(webView)
        let re = AuthRequest()
        let responseHandler = URLSessionDataTaskResponse(serializeJSON: true) {
            (json: Any) -> Any in
            print(json)
        }
        let task = URLSession.shared.fetch(urlRequest: re.buildRequest()!, responseHandler: responseHandler) { (result) in
            
        }
        //task.resume()
        webView.loadRequest(re.buildRequest()!)
        webView.delegate = self
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let range = request.url?.absoluteString.range(of: "https://mysporityfapp")  {
            
            if range.lowerBound.encodedOffset == 0 {
                if let url = request.url?.absoluteString {
                    let code = getQueryStringParameter(url: url, param: "code")
                    let re = AuthToken(code: code!)
                    let responseHandler = URLSessionDataTaskResponse(serializeJSON: true) {
                        (json: Any) -> Any in
                        print(json)
                    }
                    let task = URLSession.shared.fetch(urlRequest: re.buildRequest()!, responseHandler: responseHandler) { (result) in
                        print(result)
                    }
                    task.resume()
                }
            }
            
        }
        return true
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AlbumTableViewCell.albumTableViewCellIdentifier, for: indexPath) as! AlbumTableViewCell
        cell.configureCell(album: albums[indexPath.row], row: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dataSource.heightForRowAtIndexPath(indexpath: indexPath)
    }
   
    func didFetchAlbums(coordinator: AlbumDataCoordinator, albums: [Album]?, error: ErrorViewModel?) {
        if let albums = albums {
            self.albums = albums
            self.tableView.reloadData()
        }
    }
}

